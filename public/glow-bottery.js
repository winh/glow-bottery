const tileSize = 100;

function addGlow(element) {
  element.innerHTML += '<div class="glow"></div>';
}

function createBot() {
  const bot = createDiv();
  bot.classList.add('bot');
  addGlow(bot);
  return bot;
}

function createDiv() {
  const div = document.createElement('div');
  div.style.height = `${tileSize}px`;
  div.style.position = 'absolute';
  div.style.width = `${tileSize}px`;
  return div;
}

function createItem() {
  const itemContainer = createDiv();

  const itemSize = tileSize / 10;

  const item = createDiv();
  item.classList.add('item');
  addGlow(item);

  item.style.height = `${itemSize}px`;
  item.style.left = '50%';
  item.style.marginLeft = `-${itemSize / 2}px`;
  item.style.marginTop = `-${itemSize / 2}px`;
  item.style.top = '50%';
  item.style.width = `${itemSize}px`;

  itemContainer.appendChild(item);

  return itemContainer;
}

function createPowerSource() {
  const powerSource = createDiv();
  powerSource.classList.add('power-source', 'pulsating');
  addGlow(powerSource);
  return powerSource;
}

function createTarget() {
  const target = createDiv();
  target.classList.add('target', 'pulsating');
  addGlow(target);
  return target;
}

function setPosition(element, x, y) {
  element.style.left = `${x * tileSize}px`;
  element.style.top = `${y * tileSize}px`;
}

const bot = createBot();
const item = createItem();
const powerSource = createPowerSource();
const target = createTarget();

function loadGame() {

  const body = document.querySelector('body');

  setPosition(bot, 4, 4);
  body.appendChild(bot);

  setPosition(item, 1, 2);
  body.appendChild(item);

  setPosition(powerSource, 2, 6);
  body.appendChild(powerSource);

  setPosition(target, 1, 2);
  body.appendChild(target);
}

document.addEventListener('DOMContentLoaded', loadGame);

